package com.atguigu.eduservice.entity.subject;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @className: OneSubject
 * @Description: 一级分类
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/12/6 8:29
 */
@Data
public class OneSubject {
    private String id;
    private String title;

    //一个一级分类有多个二级分类
    private List<TwoSubject> children = new ArrayList<>();
    
}