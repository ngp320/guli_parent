package com.atguigu.eduservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @className: EduApplication
 * @Description: 启动类
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/11/17 20:09
 */
@SpringBootApplication
//扫描包名前缀为com.atguigu的包(例如跨文件扫service_base的SwaggerConfig.config)
//文件管理权限的 在同一个包的不同类 default 概念类似. target是生成出的文件结构, 可以看出是以包分类
@ComponentScan(basePackages = {"com.atguigu"})
@EnableSwagger2
public class EduApplication {
    public static void main(String[] args) {
        SpringApplication.run(EduApplication.class, args);
    }
}