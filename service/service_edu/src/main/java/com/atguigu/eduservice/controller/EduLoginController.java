package com.atguigu.eduservice.controller;

import com.atguigu.commonutils.R;
import org.springframework.web.bind.annotation.*;

/**
 * @className: EduLoginController
 * @Description: TODO
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/11/27 10:13
 */
@RestController
@RequestMapping("/eduservice/user")
@CrossOrigin //解决跨域
public class EduLoginController {
    /**
     * login
     */
    @PostMapping("login")
    public R login() {
        return R.ok().data("token", "admin");
    }

    /**
     * info
     */
    @GetMapping("info")
    public R info() {

        return R.ok().data("roles", "[admin]")
                .data("name", "admin")
                .data("avatar", "https://pic4.zhimg.com/v2-de34119300bb58939b1918fd78a89173_is.jpg");
    }
}