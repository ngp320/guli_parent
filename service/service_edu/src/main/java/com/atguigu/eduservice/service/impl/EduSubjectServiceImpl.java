package com.atguigu.eduservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.atguigu.eduservice.entity.EduSubject;
import com.atguigu.eduservice.entity.excel.SubjectData;
import com.atguigu.eduservice.entity.subject.OneSubject;
import com.atguigu.eduservice.entity.subject.TwoSubject;
import com.atguigu.eduservice.listener.SubjectExcelListener;
import com.atguigu.eduservice.mapper.EduSubjectMapper;
import com.atguigu.eduservice.service.EduSubjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author ngp320
 * @since 2020-12-04
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {
    /**
     * 添加课程分类
     *
     * @param file
     * @param subjectService
     */
    @Override
    public void saveSubject(MultipartFile file, EduSubjectService subjectService) {
        try {
            //文件输入流
            InputStream in = file.getInputStream();

            EasyExcel.read(in, SubjectData.class, new SubjectExcelListener(subjectService)).sheet().doRead();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 课程分类列表
     *
     * @return
     */
    @Override
    public List<OneSubject> getAllOneTwoSubject() {
        //查询一级分类 parent_id=0
        QueryWrapper<EduSubject> wrapperOne = new QueryWrapper<>();
        wrapperOne.eq("parent_id", 0);
        //封装了ServiceImpl中封装了baseMapper
        //this.baseMapper也可
        List<EduSubject> oneSujectList = baseMapper.selectList(wrapperOne);

        //查询二级分类
        QueryWrapper<EduSubject> wrapperTwo = new QueryWrapper<>();
        wrapperOne.ne("parent_id", 0);
        List<EduSubject> twoSujectList = baseMapper.selectList(wrapperTwo);

        //创建list集合, 用于存储最终封装数据
        ArrayList<OneSubject> finalSubjectList = new ArrayList<>();

        //封装
        for (int i = 0; i < oneSujectList.size(); i++) {
            //遍历eduSubject放到oneSubject类中
            //之前查询结构都是 eduSubject数据类型, 这里进行封装
            EduSubject oneEduSubject = oneSujectList.get(i);
            //封装一级分类
            OneSubject oneSubject = new OneSubject();
                /*oneSubject.setId(oneEduSubject.getId());
                oneSubject.setTitle(oneEduSubject.getTitle());*/
            //简化上面两行
            BeanUtils.copyProperties(oneEduSubject, oneSubject);

            //封装二级分类
            ArrayList<TwoSubject> twoFinalSubjectList = new ArrayList<>();
            for (int j = 0; j < twoSujectList.size(); j++) {
                EduSubject twoEduSubject = twoSujectList.get(j);
                //封装二级分类到一级分类
                if (twoEduSubject.getParentId().equals(oneEduSubject.getId())) {
                    TwoSubject twoSubject = new TwoSubject();
                    BeanUtils.copyProperties(twoEduSubject, twoSubject);
                    twoFinalSubjectList.add(twoSubject);
                }
            }
            oneSubject.setChildren(twoFinalSubjectList);
            finalSubjectList.add(oneSubject);
        }
        return finalSubjectList;
    }
}
