package com.atguigu.eduservice.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.atguigu.eduservice.entity.EduSubject;
import com.atguigu.eduservice.entity.excel.SubjectData;
import com.atguigu.eduservice.service.EduSubjectService;
import com.atguigu.servicebase.exceptionhandler.GuliException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * @className: SubjectExcelListener
 * @Description: 因为SubjectExcelListener不能交给spring进行管理, 需要自己手动new,不能注入其他对象, 也就不能实现数据库操作
 * * 所以需要手动把依赖传递过来
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/12/4 12:12
 */
public class SubjectExcelListener extends AnalysisEventListener<SubjectData> {
    public EduSubjectService subjectService;

    public SubjectExcelListener() {
    }

    public SubjectExcelListener(EduSubjectService subjectService) {
        this.subjectService = subjectService;
    }

    /**
     * 一行一行读取excel
     *
     * @param subjectData
     * @param analysisContext
     */
    @Override
    public void invoke(SubjectData subjectData, AnalysisContext analysisContext) {
        if (subjectData == null) {
            throw new GuliException(20001, "文件数据为空");
        }
        // 一行一行读取, 第一个值一级分类, 第二值二级分类
        /**
         * 添加一级分类, 每一行的第一列
         */
        String columnOneContent = subjectData.getColumnOneSubject();
        EduSubject existColumnOneSubject = this.existColumnOneSubject(subjectService, columnOneContent);
        //没有相同的一级分类
        if (existColumnOneSubject == null) {
            existColumnOneSubject = new EduSubject();
            existColumnOneSubject.setParentId("0");
            existColumnOneSubject.setTitle(columnOneContent);
            subjectService.save(existColumnOneSubject);
        }
        /**
         * 添加二级分类, 每一行的第二列
         */
        String pid = existColumnOneSubject.getId();
        String columnTwoSubject = subjectData.getColumnTwoSubject();
        EduSubject existColumnTwoSubject = this.existColumnTwoSubject(subjectService, columnTwoSubject, pid);
        //没有相同的二级分类
        if (existColumnTwoSubject == null) {
            existColumnTwoSubject = new EduSubject();
            existColumnTwoSubject.setParentId(pid);
            existColumnTwoSubject.setTitle(columnTwoSubject);
            subjectService.save(existColumnTwoSubject);
        }


    }

    //判断一级分类不能重复添加
    private EduSubject existColumnOneSubject(EduSubjectService subjectService, String subject) {
        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("title", subject);
        wrapper.eq("parent_id", 0);
        EduSubject oneSubject = subjectService.getOne(wrapper);
        return oneSubject;
    }

    //判断二级分类不能重复添加
    private EduSubject existColumnTwoSubject(EduSubjectService subjectService, String subject, String pid) {
        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("title", subject);
        wrapper.eq("parent_id", pid);
        EduSubject twoSubject = subjectService.getOne(wrapper);
        return twoSubject;
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}