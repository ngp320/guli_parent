package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.entity.vo.TeacherQuery;
import com.atguigu.eduservice.service.EduTeacherService;
import com.atguigu.servicebase.exceptionhandler.GuliException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author ngp320
 * @since 2020-11-17
 */
@Api(tags = "讲师管理")
@RestController
@RequestMapping("/eduservice/teacher")
@CrossOrigin
public class EduTeacherController {

    /**
     * 访问地址 http://localhost:8001/eduservice/teacher/findAll
     * service注入
     */
    @Autowired
    private EduTeacherService teacherService;

    @ApiOperation(value = "所有讲师列表")
    @GetMapping("findAll")
    public R findAllTeacher() {
        List<EduTeacher> list = teacherService.list(null);
        return R.ok().data("items", list);
    }

    @ApiOperation(value = "根据ID逻辑删除讲师", notes = "根据ID逻辑删除讲师notes")
    @DeleteMapping("{id}")
    public R removeTeacher(@ApiParam(name = "id", value = "讲师ID", required = true) @PathVariable("id") String id) {
        boolean flag = teacherService.removeById(id);
        if (flag) {
            return R.error();
        } else {
            return R.error();
        }
    }


    @ApiOperation(value = "分页查询讲师方法")
    @GetMapping("pageTeacher/{currentPage}/{pageSize}")
    public R pageListTeacher(@PathVariable long currentPage,
                             @PathVariable long pageSize) {
        try {

            int i = 10 / 0;
        } catch (Exception e) {
            //手动抛出自定义异常
            throw new GuliException(200001, "执行了自定义异常处理..");

        }
        //创建page对象
        Page<EduTeacher> page = new Page<>(currentPage, pageSize);
        //调用方法实现分页
        //调用方法时候, 底层封装, 把分页所有数据分装到page对象里
        teacherService.page(page, null);

        long total = page.getTotal();
        List<EduTeacher> records = page.getRecords();

        Map map = new HashMap();
        map.put("total", total);
        map.put("records", records);
        return R.ok().data(map);
//        return R.ok().data("total", total).data("records", records);
    }

    @ApiOperation(value = "条件查询带分页的方法")
   /* @GetMapping("pageTeacherCondition/{currentPage}/{pageSize}")
    public R pageTeacherCondition(@PathVariable long currentPage,
                                  @PathVariable long pageSize,
                                  TeacherQuery teacherQuery) {*/
    //@RequestBody(required = false) 需要用post请求
    @PostMapping("pageTeacherCondition/{currentPage}/{pageSize}")
    public R pageTeacherCondition(@PathVariable long currentPage,
                                  @PathVariable long pageSize,
                                  @RequestBody(required = false) TeacherQuery teacherQuery) {
        //创建page对象
        Page<EduTeacher> page = new Page<>(currentPage, pageSize);

        //构建条件
        QueryWrapper<EduTeacher> wrapper = new QueryWrapper<>();
        String name = teacherQuery.getName();
        Integer level = teacherQuery.getLevel();
        String begin = teacherQuery.getBegin();
        String end = teacherQuery.getEnd();
        //不为空则拼接条件
        // org.springframework.util.StringUtils;
        if (!StringUtils.isEmpty(name)) {
            wrapper.like("name", name);
        }
        if (!StringUtils.isEmpty(level)) {
            wrapper.eq("level", level);
        }
        if (!StringUtils.isEmpty(begin)) {
            //属性名称
            wrapper.ge("gmt_create", begin);
        }
        if (!StringUtils.isEmpty(end)) {
            wrapper.le("gmt_create", end);
        }
        //排序 (按创建时间降序排列)
        wrapper.orderByDesc("gmt_create");
        //实现条件分页
        teacherService.page(page, wrapper);

        long total = page.getTotal();
        List<EduTeacher> records = page.getRecords();
        return R.ok().data("total", total).data("records", records);
    }

    @ApiOperation(value = "添加讲师的方法")
    @PostMapping("addTeacher")
    public R addTeacher(@RequestBody EduTeacher eduTeacher) {
        boolean save = teacherService.save(eduTeacher);
        if (save) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    @ApiOperation(value = "根据讲师ID进行查询")
    @GetMapping("getTeacher/{id}")
    public R getTeacher(@PathVariable String id) {
        EduTeacher eduTeacher = teacherService.getById(id);
        return R.ok().data("teacher", eduTeacher);
    }


    @ApiOperation(value = "讲师修改")
    @PostMapping("updateTeacher")
    public R updateTeacher(
            @ApiParam(name = "EduTeacher", value = "讲师对象", required = true)
            @RequestBody EduTeacher eduTeacher) {
        boolean flag = teacherService.updateById(eduTeacher);
        if (flag) {
            return R.ok();
        } else {
            return R.error();
        }
    }


}

