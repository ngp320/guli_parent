package com.atguigu.eduservice.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @className: SubjectData
 * @Description: 创建跟excel对应的实体类
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/12/4 12:09
 */
@Data
public class SubjectData {
    @ExcelProperty(index = 0)
    private String columnOneSubject;
    @ExcelProperty(index = 1)
    private String columnTwoSubject;
}