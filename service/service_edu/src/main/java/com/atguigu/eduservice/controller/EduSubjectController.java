package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.subject.OneSubject;
import com.atguigu.eduservice.service.EduSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author ngp320
 * @since 2020-12-04
 */
@RestController
@RequestMapping("/eduservice/subject")
@CrossOrigin
public class EduSubjectController {

    @Autowired
    private EduSubjectService subjectService;


    /**
     * //添加课程分类
     * //获取上传过来的文件, 把文件内容读出来
     *
     * @param file
     * @return
     */
    @PostMapping("addSubject")
    public R addSubject(MultipartFile file) {
        //上床过来excel文件
        subjectService.saveSubject(file, subjectService);
        return R.ok();
    }

    /**
     * 课程分类列表(属性结构 对应vue element-ui中的)
     * 难点: 返回对应格式数据(树形结构)
     * 解决方案:
     * * 第一步 创建对应实体类
     * * 第二步 在两个实体类之间表示关系
     * * 第三步 编写具体封装代码
     */
    @GetMapping("getAllSubject")
    public R getAllSubject() {
        List<OneSubject> list = subjectService.getAllOneTwoSubject();
        return R.ok().data("list", list);
    }
}

