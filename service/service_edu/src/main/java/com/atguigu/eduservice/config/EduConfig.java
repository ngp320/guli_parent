package com.atguigu.eduservice.config;

import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.injector.LogicSqlInjector;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @className: EduConfig
 * @Description: 配置扫描mapper
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/11/17 20:11
 */
@Configuration
@MapperScan("com.atguigu.eduservice.mapper")
public class EduConfig {
    /**
     * 逻辑删除插件
     * 配置步骤:
     * * 1 配置文件中添加插件
     * * 2 实体类isDeleted添加@TableLogic注解
     * * 3 controller中编写删除方法(12配好后删除自动变逻辑删除)
     * * 4 浏览器只能测试get和post, 无法测试delete(行业规范)
     */
    @Bean
    public ISqlInjector sqlInjector() {
        return new LogicSqlInjector();
    }

    /**
     * mp分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}