package com.atguigu.eduservice.entity.subject;

import lombok.Data;

/**
 * @className: TwoSubject
 * @Description: 二级分类
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/12/6 8:30
 */
@Data
public class TwoSubject {
    private String id;
    private String title;
}