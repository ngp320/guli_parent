package com.atguigu.demo.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.Map;

/**
 * @className: ExcelListener
 * @Description: TODO
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/12/4 11:58
 */
public class ExcelListener extends AnalysisEventListener<DemoData> {

    /**
     * 一行一行读取
     *
     * @param data
     * @param analysisContext
     */
    @Override
    public void invoke(DemoData data, AnalysisContext analysisContext) {
        System.out.println("*****" + data);

    }

    /**
     * 读取表头内容
     *
     * @param headMap
     * @param context
     */
    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        System.out.println("表头:" + headMap);
    }

    /**
     * 读取完成之后
     *
     * @param analysisContext
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}