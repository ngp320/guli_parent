package com.atguigu.demo.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @className: DemoData
 * @Description: TODO
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/12/4 11:47
 */
@Data
public class DemoData {
    //设置excel表头内容
    @ExcelProperty("学生编号")
    private Integer sno;
    @ExcelProperty("学生姓名")
    private String name;
}