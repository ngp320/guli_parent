package com.atguigu.demo.excel;

import com.alibaba.excel.EasyExcel;

import java.util.ArrayList;
import java.util.List;

/**
 * @className: TestEasyExcel
 * @Description: 写操作与读操作
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/12/4 11:50
 */
public class TestEasyExcel {
    public static void main(String[] args) {
        /**
         * 写的操作
         */
        /*

        //1 设置写入文件夹地址和excel文件名称
        String filename = "D:\\write.xlsx";
        //2 调用easyexcel里面的方法进行
        //write方法: 第一个参数 文件路径名称, 第二个参数 实体类class
        EasyExcel.write(filename, DemoData.class).sheet("学生列表").doWrite(getData());*/

        /**
         * 读操作
         */
        String filename = "D:\\write.xlsx";
        EasyExcel.read(filename, DemoData.class, new ExcelListener()).sheet().doRead();
    }

    //创建方法返回list集合
    private static List<DemoData> getData() {
        ArrayList<DemoData> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            DemoData data = new DemoData();
            data.setSno(i);
            data.setName("lucy" + i);
            list.add(data);
        }
        return list;
    }

}