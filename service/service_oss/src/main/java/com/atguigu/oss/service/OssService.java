package com.atguigu.oss.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;

/**
 * @className: OssService
 * @Description: TODO
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/12/4 9:24
 */
public interface OssService {
    String uploadFileAvatar(MultipartFile file) throws FileNotFoundException;
}