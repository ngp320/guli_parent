package com.atguigu.oss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @className: OssApplication
 * @Description: 启动的时候, 找数据库配置失败, 但是上传不需要数据库
 * * 解决方式 1 添加数据库
 * * 解决方式 2 在启动类上添加属性, 不去加载数据库配置
 * * * @SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/12/4 8:41
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@ComponentScan(basePackages = {"com.atguigu"})
public class OssApplication {
    public static void main(String[] args) {
        SpringApplication.run(OssApplication.class, args);
    }
}