package com.atguigu.oss.contoller;

import com.atguigu.commonutils.R;
import com.atguigu.oss.service.OssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;

/**
 * @className: OssController
 * @Description: TODO
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/12/4 9:24
 */
@RestController
@RequestMapping("/eduoss/fileoss")
@CrossOrigin
public class OssController {

    @Autowired
    private OssService ossService;

    /**
     * 上传头像的方法
     *
     * @param file
     * @return
     */
    @PostMapping
    public R uploadOssFile(MultipartFile file) throws FileNotFoundException {
        System.out.println(file);
        //返回oss存储的路径
        String url = ossService.uploadFileAvatar(file);
        //获取上传文件 MultipartFile file
        return R.ok().data("url", url);
    }

}