package com.atguigu.oss.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @className: ConstantPropertiesUtils
 * @Description: 读取配置类常量.
 * * @Value  读取配置文件
 * * implements InitializingBean在属性被输入后, 再执行一个方法
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/12/4 9:12
 */
@Component
public class ConstantPropertiesUtils implements InitializingBean {
    @Value("${aliyun.oss.file.endpoint}")
    private String endpoint;
    @Value("${aliyun.oss.file.keyid}")
    private String keyId;
    @Value("${aliyun.oss.file.keysecret}")
    private String KeySecret;
    @Value("${aliyun.oss.file.bucketname}")
    private String bucketName;

    /**
     * 定义公开静态常量
     */
    public static String END_POINT;
    public static String ACCESS_KEY_ID;
    public static String ACCESS_KEY_SECRET;
    public static String BUCKET_NAME;

    @Override
    public void afterPropertiesSet() throws Exception {
        END_POINT = endpoint;
        ACCESS_KEY_ID = keyId;
        ACCESS_KEY_SECRET = KeySecret;
        BUCKET_NAME = bucketName;
    }
}