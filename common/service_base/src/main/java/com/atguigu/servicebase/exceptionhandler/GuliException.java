package com.atguigu.servicebase.exceptionhandler;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @className: GuliException
 * @Description: 自定义异常
 * * 第一步 创建自定义异常类(GuliException)继承RuntimeException, 写异常属性
 * * 第二步 在统一异常类(GlobalExceptionHandler)添加规则
 * * 第三步 执行自定义异常(手动抛出)
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/11/20 3:14
 */
@Data   //生成getter, setter
@AllArgsConstructor //生成有参构造
@NoArgsConstructor  //生成无参构造
public class GuliException extends RuntimeException {

    private Integer code; //状态码

    private String msg; //异常信息

}