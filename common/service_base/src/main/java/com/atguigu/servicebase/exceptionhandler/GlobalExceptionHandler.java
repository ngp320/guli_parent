package com.atguigu.servicebase.exceptionhandler;

import com.atguigu.commonutils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @className: GlobalExceptionHandler
 * @Description: 全局异常处理类
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/11/20 2:49
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    // 1 全局异常
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public R error(Exception e) {
        e.printStackTrace();
        return R.error().message("执行了全局异常处理..");

    }

    // 2 特定异常
    @ExceptionHandler(ArithmeticException.class)
    @ResponseBody
    public R error(ArithmeticException e) {
        e.printStackTrace();
        return R.error().message("执行了ArithmeticException异常处理..");

    }

    // 2 自定义异常
    @ExceptionHandler(GuliException.class)
    @ResponseBody
    public R error(GuliException e) {
        log.error(e.getMsg());
        e.printStackTrace();
        //GuliException中有code和msg, msg是为了与原R中的message区分
        return R.error().code(e.getCode()).message(e.getMsg());

    }

}