package com.atguigu.commonutils;

/**
 * @className: ResultCode
 * @Description: 返回状态码
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/11/17 21:12
 */
public class ResultCode {

    public static Integer SUCCESS = 20000; //成功

    public static Integer ERROR = 20001; //失败
}