package com.atguigu.commonutils;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @className: R
 * @Description: 统一返回结果的类
 * @version: v1.８.0
 * @author: ngp320
 * @date: 2020/11/17 21:13
 */
@Data
public class R {

    @ApiModelProperty(value = "是否成功")
    private Boolean success;

    @ApiModelProperty(value = "返回码")
    private Integer code;

    @ApiModelProperty(value = "返回消息")
    private String message;

    @ApiModelProperty(value = "返回数据")
    private Map<String, Object> data = new HashMap<String, Object>();


    /**
     * //把构造方法私有
     * <p>
     * 把构造函数设为私有的，就是不希望外界调用构造函数。
     * <p>
     * 不允许外界调用构造函数，那怎么新建对象呢？
     * <p>
     * 一种情况是直接不允许外界创建新的对象，所有对象都是这个类本身管理。
     * 最常见的就是单例模式，只允许有一个实例，自然就不允许外界创建新对象了。
     * <p>
     * 另一种情况是，类的作者不希望通过「构造函数」来向外暴露创建对象的接口，这是一种代码风格。
     * 比如说，new ArrayList<Integer>(10)是创建初始容量为 10 的列表，但可能会被误认为是创建一个列表，第一个元素为 10。
     * 为了避免这种可能出现的歧义，让创建对象的过程更清晰，可以用静态方法取代构造函数，只对外暴露静态方法创建对象的接口。
     * 比如，用ArrayList.of(1,2,3)表示用多个元素初始化列表，用ArrayList.withCapacity(10)表示创建指定初始容量的列表。
     * <p>
     * 作者：CNife
     * 链接：https://www.zhihu.com/question/396362540/answer/1241221740
     * 来源：知乎
     * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
     */
    private R() {
    }

    //成功静态方法
    public static R ok() {
        R r = new R();
        r.setSuccess(true);
        r.setCode(ResultCode.SUCCESS);
        r.setMessage("成功");
        return r;
    }

    //失败静态方法
    public static R error() {
        R r = new R();
        r.setSuccess(false);
        r.setCode(ResultCode.ERROR);
        r.setMessage("失败");
        return r;
    }

    public R success(Boolean success) {
        this.setSuccess(success);
        // this代表R对象,
        // 为了做到 链式编程 R.ok().success()
        // R.ok().message()
        // R.ok().code()
        // R.ok().data()
        return this;
    }

    public R message(String message) {
        this.setMessage(message);
        return this;
    }

    public R code(Integer code) {
        this.setCode(code);
        return this;
    }

    public R data(String key, Object value) {
        this.data.put(key, value);
        return this;
    }

    public R data(Map<String, Object> map) {
        this.setData(map);
        return this;
    }
}
